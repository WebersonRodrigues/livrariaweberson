const { check } = require('express-validator/check');

//Não criei as propriedades do modelo proporital, como o foco não é deixar o código da melhor forma possível
//e sim  aprender novas tecnologias, fiz somente o necessário. Código pode melhor muito ainda.
class Livro {
    static validacoes() {
        return [
            check('titulo').isLength({ min: 5 }).withMessage('O título precisa ter no mínimo 5 caracteres!'),
            check('preco').isCurrency().withMessage('O preço precisa ter um valor monetário válido!')
        ];
    }
}

module.exports = Livro;